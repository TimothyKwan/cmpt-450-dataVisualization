# CMPT450WebDataAnalyze
Created by Chris Smith and Timothy Kwan for CMPT 450
## Dependencies
- Pandas
- Plotly.Graph_Objects
- Dash
- Dash_Bootstrap_Components
- Dash_Core_Componets
- Dash_HTML_components
## How to Run
This application was devoloped in PyCharm Professional in Python. If you're going to use PyCharm to run this, make sure you cerate the virtual enviroment first and make sure your virutual enviroment has the above dependencies installed. Once all dependencies are installed, run the file 'main.py' to start the Dash server hosting the application on your local machine. The IP address to go to the application should printed out when the server starts, if not it should be http://127.0.0.1:8050/.
## Other Included Files
canadaCitiesJoined.csv: Has Geographic information related to the cities.  
masterData.csv: Contains all the data used for all of the attributes shown in the application for each city presented.  
Assests: This contains the files for the images used along with their attributions.  
