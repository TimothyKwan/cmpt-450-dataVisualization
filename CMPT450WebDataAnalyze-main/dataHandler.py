import pandas as pd

'''
This object contains all the data for 450 Cities Dashboard. When this object is created all necessary
calculations are done immediately so later calls will not slow down any gets
'''
class CityData:
    def __init__(self):
        # Load data into dataframe
        self._df = pd.read_csv('masterData.csv')

        # Lowest Value Ranked 1
        self._df['Rent_Rank'] = self._df['Rent'].rank(method='dense', ascending=True).astype(int)
        self._df['Unemployment_Rate_Rank'] = self._df['Unemployment_Rate'].rank(method='dense', ascending=True).astype(int)
        self._df['GHG_Rank'] = self._df['GHG'].rank(method='dense', ascending=True).astype(int)

        # Highest Value Ranked 1
        self._df['Num_Restaurants_Rank'] = self._df['Num_Restaurants'].rank(method='dense', ascending=False).astype(int)
        self._df['Median_Income_Rank'] = self._df['Median_Income'].rank(method='dense', ascending=False).astype(int)
        self._df['Total_Population_Rank'] = self._df['Total_Population'].rank(method='dense', ascending=False).astype(int)
        self._df['Population_Density_Rank'] = self._df['Population_Density'].rank(method='dense', ascending=False).astype(int)

    '''
    Private function that gets and constructs the information for returning a {city, value, rank}
    Parameters: city - The city to search for
                valueColumn - The name of the column containing the value interested in
                rankColumn - The name of the column containing the rank interested in
    Returns: If city does not exist, return None, else a dictionary in the structure {city, value, rank}
    '''
    def _getValueRank_(self, city, valueColumn, rankColumn):
        if city not in self._df['City'].values:
            return None
        else:
            df = self._df[['City', valueColumn, rankColumn]]
            value = df.loc[df['City'] == city, valueColumn].values[0]
            rank = df.loc[df['City'] == city, rankColumn].values[0]
            return {'city': city, 'value': value, 'rank': rank}

    '''
    Private function that gets and constructs the information for returning a {city, value}
    Parameters: city - The city to search for
                valueColumn - The name of the column containing the value interested in
    Returns: If city does not exist, return None, else a dictionary in the structure {city, value}
    '''
    def _getValue(self, city, valueColumn):
        if city not in self._df['City'].values:
            return None
        else:
            df = self._df[['City', valueColumn]]
            value = df.loc[df['City'] == city, valueColumn].values[0]
            return {'city': city, 'value': value}

    '''
    Gets all the cities coordinates of cities in a dictionary in
    the form{'cityName': {'cityName': cityName, value: (lat, lng)}}
    Parameters: None
    Returns: A dictionary in the form {'cityName': {'cityName': cityName, value: (lat, lng)}}
    '''
    def getAllCityCord(self):
        ret = dict()
        df = self._df['City']
        for city in df.values:
            lat = self._getValue(city, 'Lat')['value']
            lng = self._getValue(city, 'Lng')['value']
            ret[city] = {'city': city, 'value': (lat,  lng)}
        return ret

    '''
    Gets a dictionary in the form {cityName: value} where the value is determined from columnName given
    Parameters: columnNames - The name of the column, from masterData.csv, you want the values from
    Returns: A dictionary in the form {cityName: value}
    '''
    def getColumn(self, columnName):
        df = self._df[['City', columnName]]
        return dict(df[['City', columnName]].values)

    '''
    Gets a dictionary in the form {city, value} where the value is coordinates of the city queried
    Parameters: city - The name of the city you want
    Returns: A dictionary in the form {cityName: value}
    '''
    def getCityCord(self, city):
        lat = self._getValue(city, 'Lat')['value']
        lng = self._getValue(city, 'Lng')['value']
        return {'city': city, 'value': (lat, lng)}

    '''
    Gets the average rent of a city and the rank of the rent compared to other cities
    Parameters: city - A String that is the given cities name
    Returns: If city does not exist, return None, else a dictionary in the structure {city, value, rank}
    '''
    def getRent(self, city):
        return self._getValueRank_(city, 'Rent', 'Rent_Rank')

    '''
    Gets the number of restaurants in a city and the rank of the rent compared to other cities
    Parameters: city - A String that is the given cities name
    Returns: If city does not exist, return None, else a dictionary in the structure {city, value, rank}
    '''
    def getNumRestaurants(self, city):
        return self._getValueRank_(city, 'Num_Restaurants', 'Num_Restaurants_Rank')

    '''
    Gets the unemployment in a city and the rank of the rent compared to other cities
    Parameters: city - A String that is the given cities name
    Returns: If city does not exist, return None, else a dictionary in the structure {city, value, rank}
    '''
    def getUnemploymentRate(self, city):
        return self._getValueRank_(city, 'Unemployment_Rate', 'Unemployment_Rate_Rank')

    '''
    Gets the GHG emitted in a city and the rank of the rent compared to other cities
    Parameters: city - A String that is the given cities name
    Returns: If city does not exist, return None, else a dictionary in the structure {city, value, rank}
    '''
    def getGHG(self, city):
        return self._getValueRank_(city, 'GHG', 'GHG_Rank')

    '''
    Gets the median income in a city and the rank of the rent compared to other cities
    Parameters: city - A String that is the given cities name
    Returns: If city does not exist, return None, else a dictionary in the structure {city, value, rank}
    '''
    def getMedianIncome(self, city):
        return self._getValueRank_(city, 'Median_Income', 'Median_Income_Rank')

    '''
    Gets the total population of a city and the rank of the rent compared to other cities
    Parameters: city - A String that is the given cities name
    Returns: If city does not exist, return None, else a dictionary in the structure {city, value, rank}
    '''
    def getTotalPopulation(self, city):
        return self._getValueRank_(city, 'Total_Population', 'Total_Population_Rank')

    '''
    Gets the population density of a city and the rank of the rent compared to other cities
    Parameters: city - A String that is the given cities name
    Returns: If city does not exist, return None, else a dictionary in the structure {city, value, rank}
    '''
    def getPopulationDensity(self, city):
        return self._getValueRank_(city, 'Population_Density', 'Population_Density_Rank')
