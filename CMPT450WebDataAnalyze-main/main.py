# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import pandas as pd
import plotly.graph_objects as go
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
import dataHandler as DH
from dash.dependencies import Input, Output, State

mapbox_token = 'pk.eyJ1IjoidGltb3RoeWt3YW4iLCJhIjoiY2ttMDNkZHd4MG9rYTJ2cDR1cjN6MTh5cSJ9.asEkwPT9x7SBRjh0ehzQrg'
darkBGC = '#272B30'
lighterBGC = '#3A3F44'


# get the data and generate the plotly map object
def generateMap(data, selectedCity):
    colorsList = []
    print(selectedCity)
    for i in data['city']:
        if len(selectedCity) > 0 and i in selectedCity:
            print("selected cities are " + i)
            colorsList.append('crimson')
        else:
            colorsList.append('lightslategray')

    fig = go.Figure(
        go.Scattermapbox(
            lat=data['lat'],
            lon=data['lng'],
            marker=go.scattermapbox.Marker(
                size=10),
            marker_color=colorsList,
            text='City name: ' + data['city'],
            hovertemplate='%{text}<extra></extra>'
        )
    )
    fig.update_layout(
        mapbox=dict(
            accesstoken=mapbox_token,
            center=go.layout.mapbox.Center(lat=55, lon=-115),
            zoom=4.25),
        margin={"r": 0, "t": 0, "l": 0, "b": 0},
        height=600,
        plot_bgcolor=lighterBGC,
        paper_bgcolor=lighterBGC
    )
    return fig


# get the file name and return the pandas object contain only alberta city
def getAlbertaCity(fileName):
    data = pd.read_csv(fileName)
    albertaCity = data[data['province_id'] == 'AB']
    return albertaCity


# input: take the pandas object
# output: a list of city names
def getCityNames(data):
    names = pd.unique(data["city"])
    return names


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    selectedCities = []
    app = dash.Dash(external_stylesheets=[dbc.themes.SLATE])
    data = getAlbertaCity("canadacitiesJoined.csv")
    handler = DH.CityData()  # reference of the handler do not create another instance *****important
    mainTitle = html.Div([dcc.Markdown('Localee', style={'fontSize': '36px', 'color': 'white'}),
                          dcc.Markdown('Created by Chris Smith and Timothy', style={'fontSize': '12px', 'color': 'white'})])
    map = html.Div([dcc.Graph(figure=generateMap(data, []))], id='mapObject')
    mapHTML = html.Div([
        mainTitle,
        map],
        style={
            'width': '100%',
            'height': '100%',
            'display': 'inline-block',
            'backgroundColor': lighterBGC,
            'padding': '0px 10px 0px 10px'
        })

    # this is for showing the selected city

    # get names & value
    cityLabel = []
    for city in getCityNames(data):
        cityLabel.append({'label': city, 'value': city})

    # dropdown, button, and text
    dropDownHTML = html.Div(
        [html.Div([dcc.Dropdown(
            id='cityDropdown',
            options=cityLabel,
            value='',
            placeholder='Select a City',
            multi=True,
            style={
                'backgroundColor': lighterBGC
            }

        )],
            style={
                'width': '45%',
                'display': 'inline-block',
                'color': 'black'
            }
        ),
            html.Div([dbc.Button(
                "Select",
                id='citySelect',
                n_clicks=0,
                outline=True,
                color='success',
                className='mr-1',
                style={
                    'margin-left': 20,
                    'margin-bottom': 25,
                    'backgroundColor': lighterBGC
                }
            )],
                style={
                    'width': '45%',
                    'margin-bottom': 5,
                    'display': 'inline-block'
                }
            )
        ],
        style={
            # 'border':'1px grey solid',
            'width': '100%',
            'padding': 5
        }
    )

    '''
    purpose: generate category the city card, which is in the first row of the result
    parameter: image name - the image name that is in the assets file
    title - title of the card
    cityName - Name of the City
    text - description of the City
    output: html object that contains the layout of the card
    '''
    def generateCategoryCard(imgName, title, cityDict, tagName):
        # handle text
        # dictionary format: "city_name":{'city': city, 'value': value, 'rank': rank}
        key = cityDict.keys()
        displayText = [title + '  ']
        # add all the rank into display text
        for i in key:
            tmpText = i + ": **" + str(cityDict[i]['rank'])
            if ((cityDict[i]['rank'] == 1)):
                tmpText += "st"
            elif ((cityDict[i]['rank'] == 2)):
                tmpText += "nd"
            elif (cityDict[i]['rank'] == 3):
                tmpText += "rd"
            else:
                tmpText += "th"
            tmpText += '**  '
            displayText.append(tmpText)

        card = html.Div([
            html.Div([
                dbc.Row([html.Img(src=app.get_asset_url(imgName),
                                  id=tagName,
                                  style={
                                      'height': '50%',
                                      'width': '50%'
                                  },
                                  n_clicks=0
                                  )],
                        justify='center'
                        )
            ]),
            dcc.Markdown(displayText)],
            style={
                'width': '22%',
                'margin-left': '5px',
                'margin-right': '5px',
                'text-align': 'center'
            },
        )
        return card


    '''
    purpose: generate the city card, which is in the first row of the result
    parameter: image name - the image name that is in the assets file
    title - title of the card
    cityName - Name of the City
    text - description of the City
    output: html object that contains the layout of the card
    '''
    def generateCityCard(imgName, title, cityName, text):
        card = html.Div([
            html.Div([
                html.Img(src=app.get_asset_url(imgName),
                         style={
                             'height': '50%',
                             'width': '50%'
                         }
                         )]),
            html.P(title),
            html.H3(cityName),
            html.P(text)
        ],
            style={
                'width': '17%',
                'margin-left': '20px',
                'margin-right': '20px'
            }
        )
        return card


    # generate the result based on the selected list of city
    def resultLayout(dataList):
        cityCards = []
        rentList = {}
        restaurantList = {}
        unemploymentList = {}
        GHGList = {}
        medianIncomeList = {}
        populationList = {}
        populatiobDensList = {}
        # go through all the city names and get all data from handler
        for i in range(len(dataList)):
            cityCards.append(generateCityCard('CityIcon.png', 'City Name:', dataList[i], 'is selected'))
            rentList[dataList[i]] = handler.getRent(dataList[i])
            restaurantList[dataList[i]] = handler.getNumRestaurants(dataList[i])
            unemploymentList[dataList[i]] = handler.getUnemploymentRate(dataList[i])
            GHGList[dataList[i]] = handler.getGHG(dataList[i])
            medianIncomeList[dataList[i]] = handler.getMedianIncome(dataList[i])
            populationList[dataList[i]] = handler.getTotalPopulation(dataList[i])
            populatiobDensList[dataList[i]] = handler.getPopulationDensity(dataList[i])

        rentCard = generateCategoryCard('for-rent.png', 'Average Rent:', rentList, 'rentButton')
        restaurantCard = generateCategoryCard('restaurant.png', 'Number of Restaurants:', restaurantList,
                                              'restaurantButton')
        umemployCard = generateCategoryCard('job-search.png', 'Unemployment Rate:', unemploymentList, 'unemployButton')
        GHGCard = generateCategoryCard('wind.png', 'GHG:', GHGList, 'GHGButton')
        incomeCard = generateCategoryCard('money.png', 'Median Income:', medianIncomeList, 'incomeButton')
        population = generateCategoryCard('user.png', 'Total Population:', populationList, 'populationButton')
        popDens = generateCategoryCard('group.png', 'Density:', populatiobDensList, 'populationDensityButton')

        # print("city cards length "+str(len(cityCards)))
        # generate the card layout
        cardLayout = html.Div(
            [
                dbc.Row([
                    rentCard, restaurantCard, umemployCard
                ],
                    justify='center',
                    style={
                        'margin-top': '5px'
                    }
                ),
                dbc.Row([
                    GHGCard, incomeCard, population, popDens
                ],
                    justify='center'
                )
            ],
            style={
                'margin-left': '20px'
            }
        )
        return cardLayout


    # result page
    def generateResult(cityList):
        tmpPage = None
        if (len(cityList) == 0):
            # print("result page is empty")
            return tmpPage
        # print("result page should shown")
        tmpPage = html.Div(
            [resultLayout(cityList)]
        )
        return tmpPage

    # update layout
    def updateLayout(map, content):
        app.layout = html.Div([
            dbc.Row([
                dbc.Col([map], width=3),
                dbc.Col([content], width=6)
            ],
                no_gutters=True
            )
        ])
        return

    # generate first page
    resultTitle = html.Div([dcc.Markdown('Select Your Cities to Explore', style={'font-size': '20px'})], id='resultTitle',)
    resultPage = html.Div(id='rankPage')
    resultGraph = html.Div(id='graphPage')
    contentHTML = html.Div(
        [resultTitle,
         dropDownHTML,
         resultPage,
         resultGraph
         ],
        id='contentPage',
        style={
            'width': '100%',
            'display': 'inline-block',
            'padding': 5,
            'margin-left': '20px',
            'color': 'white'
        }
    )


    # update the result page
    @app.callback(
        Output('rankPage', 'children'),
        [Input('citySelect', 'n_clicks')],
        [State('cityDropdown', 'value')]
    )
    def updateResult(n_clicks, city):
        selectedCities.clear()
        for i in city:
            selectedCities.append(i)
        return generateResult(city)


    # 7 update methods to update the graphs
    # input: takes a list of dictionary with the format
    # output: a dcc graph object
    # TODO: switch into tab layout
    def generateResultGraph(cityNames, valueColumn):
        # "city_name": {'city': city, 'value': value, 'rank': rank}
        values = []
        categoryText = []
        graphTitle = ""
        measurementText = ""
        # get the values base on column
        if valueColumn == 'Rent':
            graphTitle = "Average Rent Per City"
            measurementText = "Average Rent Cost (CAD)"
            categoryText = ['Rent'] * len(cityNames)
            for i in cityNames:
                values.append(handler.getRent(i)['value'])
        elif valueColumn == 'Num_Restaurants':
            measurementText = "Number of Restaurants"
            graphTitle = "Number of Good Standing Licensed Restaurants"
            categoryText = ['Restaurants'] * len(cityNames)
            for i in cityNames:
                values.append(handler.getNumRestaurants(i)['value'])
        elif valueColumn == 'Unemployment_Rate':
            measurementText = "Unemployment Rate (Lower is Better)"
            graphTitle = "Unemployment Rate Per City"
            categoryText = ['Unemploy Rate'] * len(cityNames)
            for i in cityNames:
                values.append(handler.getUnemploymentRate(i)['value'])
        elif valueColumn == 'GHG':
            measurementText = "GHG (kt)"
            graphTitle = "GHG Emission Per City"
            categoryText = ['GHG Emission'] * len(cityNames)
            for i in cityNames:
                values.append(handler.getGHG(i)['value'])
        elif valueColumn == 'Median_Income':
            measurementText = "$(CAD)"
            graphTitle = "Median Income Per City"
            categoryText = ['Median Income'] * len(cityNames)
            for i in cityNames:
                values.append(handler.getMedianIncome(i)['value'])
        elif valueColumn == 'Total_Population':
            measurementText = "Population"
            graphTitle = "Total population Per City"
            categoryText = ['Population'] * len(cityNames)
            for i in cityNames:
                values.append(handler.getTotalPopulation(i)['value'])
        elif valueColumn == 'Population_Density':
            measurementText = "Population Density (Per km^2)"
            graphTitle = "Population Density Per City"
            categoryText = ['Density'] * len(cityNames)
            for i in cityNames:
                values.append(handler.getPopulationDensity(i)['value'])

        fig = go.Figure([
            go.Bar(x=cityNames, y=values,
                   text=categoryText,
                   hovertemplate='City: %{x}<br>%{text}: %{y}</br><extra></extra>'
                   )
        ])

        if (valueColumn == "GHG"):
            fig.add_annotation(x=len(cityNames),
                               text="GHG emissions are reported only for <br>cities emitting over 50kt</br>",
                               showarrow=False,
                               )
        fig.update_layout(
            title=graphTitle,
            xaxis_title="City",
            yaxis_title=measurementText,
            font_color='white',
            plot_bgcolor=lighterBGC,
            paper_bgcolor=darkBGC,
            margin={"r": 10, "t": 40, "l": 10, "b": 10},
        )
        return fig

#incase we need icon: , 'backgroundImage':'url("/assets/for-rent.png")', 'background-repeat': 'no-repeat', 'background-position':'center', 'backgroundSize': '50px'
    # Tab
    @app.callback(
        Output('graphPage', 'children'),
        [Input('citySelect', 'n_clicks')],
        [State('cityDropdown', 'value')]
    )
    def generateCategoryGraph(n_clicks, value):
        if len(value) > 0:
            tab_style = {'padding': '5px', 'backgroundColor': lighterBGC}
            tab_selected_style = {'padding': '5px', 'backgroundColor': 'lightblue'}
            return dcc.Tabs(id='graphTabs', value='Rent', children=[
                dcc.Tab(label='Rent', children=[dcc.Graph(figure=generateResultGraph(value, 'Rent'))], style=tab_style,
                        selected_style=tab_selected_style),
                dcc.Tab(label='Number of Restaurants',
                        children=[dcc.Graph(figure=generateResultGraph(value, 'Num_Restaurants'))], style=tab_style,
                        selected_style=tab_selected_style),
                dcc.Tab(label='Unemployment Rate',
                        children=[dcc.Graph(figure=generateResultGraph(value, 'Unemployment_Rate'))], style=tab_style,
                        selected_style=tab_selected_style),
                dcc.Tab(label='GHG', children=[dcc.Graph(figure=generateResultGraph(value, 'GHG'))], style=tab_style,
                        selected_style=tab_selected_style),
                dcc.Tab(label='Median Income', children=[dcc.Graph(figure=generateResultGraph(value, 'Median_Income'))],
                        style=tab_style, selected_style=tab_selected_style),
                dcc.Tab(label='Population', children=[dcc.Graph(figure=generateResultGraph(value, 'Total_Population'))],
                        style=tab_style, selected_style=tab_selected_style),
                dcc.Tab(label='Population Density',
                        children=[dcc.Graph(figure=generateResultGraph(value, 'Population_Density'))], style=tab_style,
                        selected_style=tab_selected_style)
            ])
        else:
            return None


    # update the map when a city is selected

    @app.callback(
        Output('mapObject', 'children'),
        [Input('citySelect', 'n_clicks')],
        [State('cityDropdown', 'value')]
    )
    def updateResultMap(n_clicks, city):
        print(city)
        return dcc.Graph(figure=generateMap(data, city))


    # add everything together and show the page
    app.layout = html.Div([
        dbc.Row([
            dbc.Col([mapHTML], width=3),
            dbc.Col([contentHTML], width=6)
        ],
            no_gutters=True
        )
    ])
    app.run_server()
